from django import forms
from .models import Employee
from django.contrib.auth.forms import AuthenticationForm 


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", max_length=30, 
                               widget=forms.TextInput(attrs={'class': 'form-control', 'name': 'username'}))
    password = forms.CharField(label="Password", max_length=30, 
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'name': 'password'}))
class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        widgets={
        'name': forms.TextInput(attrs={ 'class':'form-control','placeholder': 'Campain Name'}),
        'mobilenumber':forms.TextInput(attrs={'class':'form-control','placeholder': 'Enter mobilenumber'}),
        'salary':forms.TextInput(attrs={'class':'form-control','placeholder': 'Enter your salary'}),
        'designation':forms.Select(attrs={'class':'form-control','placeholder': 'Enter designation'}),
        'email':forms.TextInput(attrs={'class':'form-control','placeholder': 'Enter email'}),
        }
        exclude=()
       

        