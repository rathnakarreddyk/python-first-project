# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

import json
from collections import defaultdict
# Create your views here.
from django.http import HttpResponse,HttpResponseRedirect,Http404
from .models import *
from django.views.decorators.csrf import csrf_exempt
from forms import EmployeeForm
from django.shortcuts import get_object_or_404,render_to_response
from django.contrib import messages
from django.contrib.auth.decorators import login_required

def index(request):
    
    return render(request,"polls/polls-list.html")

def homec(request):
    
    return render(request,"polls/home.html")
def contact(request):
    return render(request,'polls/basic.html',{'content':['If you would like to contact me, please email me.','hskinsley@gmail.com']})


@login_required(login_url="/login/")
def employee(request,pk=None,f=None):
    print request
    if pk:
      f = Employee.objects.get(pk=pk)
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = EmployeeForm(request.POST,  instance=f)
        print form
        # check whether it's valid:
        if form.is_valid():
            form.save()
            if not pk:
              messages.success(request,"New records save Successfully")
            else:
              messages.success(request,"Record updated Successfully")
            return HttpResponseRedirect('/polls/user')
        else:
          print form.errors

    # if a GET (or any other method) we'll create a blank form
    else:
      form = EmployeeForm(instance=f)

   

    return render_to_response('polls/employeeform.html',{"form":form})

@login_required(login_url='/login/')
def get_Emloyees(request):
  emp=Employee.objects.all()
  return render(request,'polls/user_page.html',{"user":emp})

def get_employeeDetails(request,pk):
  try:
    user = get_object_or_404(Employee, pk=pk)
    print user,"gjhjgh"
  except:
     raise Http404('Requested user not found.')
  
  return render(request, 'polls/basic.html',{"form":user})


@login_required(login_url='/login/')
def delete_employeeDetails(request,pk):
  print request
  if request.method=='GET':
    print request.method
    user=get_object_or_404(Employee, pk=pk)
    print user
    
    user.delete()
    message="deletee sucessfully"

    return HttpResponseRedirect('/polls/user')
  return HttpResponseRedirect('/polls/user',{"msg":message})

@login_required(login_url="/login/")
def home(request):
    print request
    return render(request,"polls/home.html")

  




