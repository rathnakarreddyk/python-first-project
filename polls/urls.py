from django.conf.urls import include, url
from django.contrib.auth.views import login

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^home/$', views.home, name='home'),
    url(r'^employeeform/$', views.employee, name='employee'),
    url(r'^contact/', views.contact, name='contact'),
    url(r'^user/', views.get_Emloyees, name='user'),
    url(r'^(?P<pk>\d+)/view$', views.get_employeeDetails, name='get_employeeDetails'), 
    url(r'^(?P<pk>\d+)/delete$', views.delete_employeeDetails, name='delete_employeeDetails'), 
    url(r'^(?P<pk>\d+)/edit$', views.employee, name='employeename'), 
   	
]